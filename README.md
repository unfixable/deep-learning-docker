# Deep Learning Docker Container

Dockerfile containing all major deep learning libraries and various other tools.

Hosted on dockerhub:

**docker pull unfixable/deeplearning:1.4**
(https://cloud.docker.com/repository/docker/unfixable/deeplearning/general)

Based on:
ufoym/deepo (https://hub.docker.com/r/ufoym/deepo/).
